"""
Represents a row in the visited_cities table.  The format is (username, city_visited, time_visited).
"""

class UserVisit(object):
    def __init__(self, username, city_visited, time_visited):
        self.username = username
        self.city_visited = city_visited
        self.time_visited = time_visited