import unittest2

import findspark
findspark.init()
from pyspark.context import SparkContext
import leastVisitedCities

from tests.data.UserVisitModel import UserVisit


class LeastVisitedCitiesTest(unittest2.TestCase):

    def setUp(self):
        self.sc = SparkContext('local[4]')

    def tearDown(self):
        self.sc.stop()

    def test_getLeastVisitedNCitiesUsingRDD(self):
        """
         Test getLeastVisitedNCitiesUsingRDD counts unique username visits
         and uses the latest visit date.
        """

        test_input = [
            UserVisit('user1', 'city1', '2014-11-18 15:04:46'),
            UserVisit('user1', 'city1', '2014-11-20 16:04:46'),
            UserVisit('user1', 'city2', '2014-11-18 17:04:46'),
            UserVisit('user2', 'city2', '2014-11-18 15:04:46'),
            UserVisit('user2', 'city4', '2014-11-30 15:04:46'),
        ]

        input_rdd = self.sc.parallelize(test_input)
        results = leastVisitedCities.getLeastVisitedNCities(input_rdd, 2)
        expected_results = {
            'city1': { 'num_of_unique_users': 1, 'last_visited': '2014-11-20 16:04:46' },
            'city4': { 'num_of_unique_users': 1, 'last_visited': '2014-11-30 15:04:46' }
        }
        self.assertUserVisitResponse(results, expected_results)

    def test_getLeastVisitedNCitiesUsingRDD_emptyRows(self):
        """
         Test getLeastVisitedNCitiesUsingRDD ignores cities with no visits.
        """

        test_input = [
            UserVisit('user1', 'city1', '2014-11-18 15:04:46'),
            UserVisit('user1', 'city1', '2014-11-20 16:04:46'),
            UserVisit('user1', 'city2', '2014-11-18 17:04:46'),
            UserVisit('user2', 'city2', '2014-11-18 15:04:46'),
            UserVisit('user3', 'city3', None),
        ]

        input_rdd = self.sc.parallelize(test_input)
        results = leastVisitedCities.getLeastVisitedNCities(input_rdd, 3)
        expected_results = {
            'city1': { 'num_of_unique_users': 1, 'last_visited': '2014-11-20 16:04:46' },
            'city2': { 'num_of_unique_users': 2, 'last_visited': '2014-11-18 17:04:46' }
        }
        self.assertUserVisitResponse(results, expected_results)

    def test_getLeastVisitedNCitiesUsingRDD_notEnoughData(self):
        """
         Test getLeastVisitedNCitiesUsingRDD works when there's less data than
         the number of cities to return
        """

        test_input = [
            UserVisit('user1', 'city1', '2014-11-18 15:04:46')
        ]

        input_rdd = self.sc.parallelize(test_input)
        results = leastVisitedCities.getLeastVisitedNCities(input_rdd, 3)
        expected_results = {
            'city1': { 'num_of_unique_users': 1, 'last_visited': '2014-11-18 15:04:46' }
        }
        self.assertUserVisitResponse(results, expected_results)

    def test_getLeastVisitedNCitiesUsingRDD_emptySet(self):
        """
         Test getLeastVisitedNCitiesUsingRDD returns 0 results when there's no data
        """

        input_rdd = self.sc.parallelize([])
        results = leastVisitedCities.getLeastVisitedNCities(input_rdd, 3)
        self.assertUserVisitResponse(results, {})

    def assertUserVisitResponse(self, actual_results, expected_results):
        self.assertEqual(len(actual_results), len(expected_results))
        for result in actual_results:
            city_name = result[0]
            num_of_unique_users, last_visited = result[1]
            self.assertTrue(city_name in expected_results)
            self.assertEqual(num_of_unique_users, expected_results[city_name]['num_of_unique_users'])
            self.assertEqual(last_visited, expected_results[city_name]['last_visited'])


if __name__ == "__main__":
    unittest2.main()
