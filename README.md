# README #

### Problem ###
Given a cassandra table schema:  
(user_name), city_visited, time_visited

In a language of your choice, implement a way to list the top 5 least visited cities by unique users and include the most recent time that it has been visited. We do not want to see unvisited cities on the list. Please use Spark for this query.

It should be optimized so that processing of a user_name / city_visited stops after it has found 5 visits for that user_name / city_visited.

### Solution ###
#### Requirements ####
* Spark 1.6.0
* Cassandra 2.2.5
* datastax:spark-cassandra-connector 1.6.0-M1-s_2.10 (this version avoids the utf-8 sql errors when reading from Cassandra)
* Python 2.7.11 :: Anaconda 2.5.0 (64-bit)


#### Setup notes ####
This was tested using 4 vms running Spark and Cassandra.  The DataStrax Cassandra connector was used to get each Spark instance to attempt to use the local Cassandra instance whenever possible.

```
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Thread(s) per core:    1
Core(s) per socket:    1
Socket(s):             4
NUMA node(s):          1
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 45
Stepping:              2
CPU MHz:               2900.000
BogoMIPS:              5800.00
Hypervisor vendor:     VMware
Virtualization type:   full
L1d cache:             32K
L1i cache:             32K
L2 cache:              256K
L3 cache:              20480K
NUMA node0 CPU(s):     0-3
```

#### Run instructions:####
Note: if you have jupiter notebook running, you may have to unset PYSPARK_DRIVER_PYTHON to run the submit job
```
bin/spark-submit --master spark://<spark-master>:7077 --packages datastax:spark-cassandra-connector:1.6.0-M1-s_2.10 --conf spark.cassandra.connection.host=<cassandra-seed> --conf spark.cassandra.auth.username=<cassandra username> --conf spark.cassandra.auth.password=<cassandra password> <project location>/leastVisitedCities.py <num of results to return, default 5>
```
Unit tests (may have to unset PYSPARK_SUBMIT_ARGS to run the tests):
```
python -m unittest discover tests/
```

#### DataFrames vs RDD ####

I originally expected DataFrames to perform better than RDDs because of the SQL optimization.  In this case at least, the RDD solution that is checked in out-performs the following code:

```
import pyspark.sql.functions as func

results = visited_cities \
    .groupBy('city_visited') \
    .agg( \
         func.countDistinct('username').alias('unique_visitors'), \
         func.max('time_visited').alias('last_time_visited')) \
    .orderBy('unique_visitors')
    .limit(5).collect()
```

#####Sample run times against Cassandra: data files are under test_data (5 partitions):#####
```
Dataframes:
100000 rows => 0.14977ms/row
10000 rows => 0.896ms/row
100 rows => 54.285ms/row

RDD:
100000 rows => 0.09671665ms/row
10000 rows => 0.876497ms/row
100 rows => 34.65691ms/row
```
This disparity is likely caused by the small amount of cities being returned.  The RDD takeOrdered function performs the takeOrdered functionality on each partition before performing it on master, so there is less sorting performed on each partition and less data being transferred to the master node.  Further performance gains on the RDD may be possible by using repartitionAndSortWithinPartitions to optimize sorting.

A note about the test data:
The following create table was used
```
CREATE TABLE visited_cities (
     username text,
     city_visited text,
     time_visited timestamp,
     PRIMARY KEY ((username), city_visited, time_visited)
);
```
To follow the data schema of (username), city_visited, time_visited and still allow for a user to visit a city more than once, I had to put city_visited and time_visited as clustering keys, which could not be null.  An alternative schema would be to add an index key per user visit or to remove time_visited as a clustering key - a user would not be able to visit the same city more than once in that case.  The spark code will filter out unvisited cities regardless (see unit tests).



DataFrame solution's explain returns:
```
== Parsed Logical Plan ==
'Sort ['unique_visitors ASC], true
+- Aggregate [city_visited#1], [city_visited#1,(count(if ((gid#25 = 1)) username#26 else null),mode=Complete,isDistinct=false) AS unique_visitors#23L,(first(if ((gid#25 = 0)) max(time_visited)#28 else null) ignore nulls,mode=Complete,isDistinct=false) AS last_time_visited#24]
   +- Aggregate [city_visited#1,username#26,gid#25], [city_visited#1,username#26,gid#25,(max(time_visited#27),mode=Complete,isDistinct=false) AS max(time_visited)#28]
      +- Expand [ArrayBuffer(city_visited#1, null, 0, time_visited#2),ArrayBuffer(city_visited#1, username#0, 1, null)], [city_visited#1,username#26,gid#25,time_visited#27]
         +- Relation[username#0,city_visited#1,time_visited#2] org.apache.spark.sql.cassandra.CassandraSourceRelation@68a4b8f5

== Analyzed Logical Plan ==
city_visited: string, unique_visitors: bigint, last_time_visited: timestamp
Sort [unique_visitors#23L ASC], true
+- Aggregate [city_visited#1], [city_visited#1,(count(if ((gid#25 = 1)) username#26 else null),mode=Complete,isDistinct=false) AS unique_visitors#23L,(first(if ((gid#25 = 0)) max(time_visited)#28 else null) ignore nulls,mode=Complete,isDistinct=false) AS last_time_visited#24]
   +- Aggregate [city_visited#1,username#26,gid#25], [city_visited#1,username#26,gid#25,(max(time_visited#27),mode=Complete,isDistinct=false) AS max(time_visited)#28]
      +- Expand [ArrayBuffer(city_visited#1, null, 0, time_visited#2),ArrayBuffer(city_visited#1, username#0, 1, null)], [city_visited#1,username#26,gid#25,time_visited#27]
         +- Relation[username#0,city_visited#1,time_visited#2] org.apache.spark.sql.cassandra.CassandraSourceRelation@68a4b8f5

== Optimized Logical Plan ==
Sort [unique_visitors#23L ASC], true
+- Aggregate [city_visited#1], [city_visited#1,(count(if ((gid#25 = 1)) username#26 else null),mode=Complete,isDistinct=false) AS unique_visitors#23L,(first(if ((gid#25 = 0)) max(time_visited)#28 else null) ignore nulls,mode=Complete,isDistinct=false) AS last_time_visited#24]
   +- Aggregate [city_visited#1,username#26,gid#25], [city_visited#1,username#26,gid#25,(max(time_visited#27),mode=Complete,isDistinct=false) AS max(time_visited)#28]
      +- Expand [ArrayBuffer(city_visited#1, null, 0, time_visited#2),ArrayBuffer(city_visited#1, username#0, 1, null)], [city_visited#1,username#26,gid#25,time_visited#27]
         +- Relation[username#0,city_visited#1,time_visited#2] org.apache.spark.sql.cassandra.CassandraSourceRelation@68a4b8f5

== Physical Plan ==
Sort [unique_visitors#23L ASC], true, 0
+- ConvertToUnsafe
   +- Exchange rangepartitioning(unique_visitors#23L ASC,200), None
      +- ConvertToSafe
         +- TungstenAggregate(key=[city_visited#1], functions=[(count(if ((gid#25 = 1)) username#26 else null),mode=Final,isDistinct=false),(first(if ((gid#25 = 0)) max(time_visited)#28 else null) ignore nulls,mode=Final,isDistinct=false)], output=[city_visited#1,unique_visitors#23L,last_time_visited#24])
            +- TungstenExchange hashpartitioning(city_visited#1,200), None
               +- TungstenAggregate(key=[city_visited#1], functions=[(count(if ((gid#25 = 1)) username#26 else null),mode=Partial,isDistinct=false),(first(if ((gid#25 = 0)) max(time_visited)#28 else null) ignore nulls,mode=Partial,isDistinct=false)], output=[city_visited#1,count#37L,first#38,valueSet#39])
                  +- TungstenAggregate(key=[city_visited#1,username#26,gid#25], functions=[(max(time_visited#27),mode=Final,isDistinct=false)], output=[city_visited#1,username#26,gid#25,max(time_visited)#28])
                     +- TungstenExchange hashpartitioning(city_visited#1,username#26,gid#25,200), None
                        +- TungstenAggregate(key=[city_visited#1,username#26,gid#25], functions=[(max(time_visited#27),mode=Partial,isDistinct=false)], output=[city_visited#1,username#26,gid#25,max#33])
                           +- Expand [ArrayBuffer(city_visited#1, null, 0, time_visited#2),ArrayBuffer(city_visited#1, username#0, 1, null)], [city_visited#1,username#26,gid#25,time_visited#27]
                              +- Scan org.apache.spark.sql.cassandra.CassandraSourceRelation@68a4b8f5[username#0,city_visited#1,time_visited#2]
```

RDD solution's debugString returns:
```
(5) PythonRDD[97] at RDD at PythonRDD.scala:43 []
 |  MapPartitionsRDD[96] at mapPartitions at PythonRDD.scala:374 []
 |  ShuffledRDD[95] at partitionBy at NativeMethodAccessorImpl.java:-2 []
 +-(5) PairwiseRDD[94] at reduceByKey at <ipython-input-13-5948d3bc0a90>:19 []
    |  PythonRDD[93] at reduceByKey at <ipython-input-13-5948d3bc0a90>:19 []
    |  MapPartitionsRDD[92] at mapPartitions at PythonRDD.scala:374 []
    |  ShuffledRDD[91] at partitionBy at NativeMethodAccessorImpl.java:-2 []
    +-(5) PairwiseRDD[90] at reduceByKey at <ipython-input-13-5948d3bc0a90>:15 []
       |  PythonRDD[89] at reduceByKey at <ipython-input-13-5948d3bc0a90>:15 []
       |  MapPartitionsRDD[68] at javaToPython at NativeMethodAccessorImpl.java:-2 []
       |  MapPartitionsRDD[67] at javaToPython at NativeMethodAccessorImpl.java:-2 []
       |  MapPartitionsRDD[66] at javaToPython at NativeMethodAccessorImpl.java:-2 []
       |  CassandraTableScanRDD[65] at RDD at CassandraRDD.scala:15 []
```