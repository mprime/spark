import sys
import time
from pyspark import SparkContext
from pyspark.sql import SQLContext


DEFAULT_NUM_OF_RESULTS = 5

def setupDataFrame(spark_context):
    """
    Get a dataframe representing the visited_cities table.
    :param spark_context: Default spark context
    :return:Spark DataFrame pointing to the visited_cities table
    """
    sql = SQLContext(spark_context)
    visited_cities = sql.read.format("org.apache.spark.sql.cassandra").\
        load(keyspace="test", table="visited_cities")
    return visited_cities

def getLeastVisitedNCities(visited_cities_rdd, num_of_results):
    """
    Given an RDD of the following structure ,
    find the least visited cities based on the number of unique users
    :param visited_cities_rdd: From the visited_cities table, format is (username, city_visited, time_visited)
    :param num_of_results: The number of cities to be returned
    :return:list of cities, format is (city_name, (num_of_unique_users, last_visited))
    """
    results_rdd = visited_cities_rdd.filter(
        # Remove cities with no visitors
        lambda row: row.time_visited != None
    ).map(
        # Restructure to a key value format
        lambda row: ((row.username, row.city_visited), row.time_visited), True
    ).reduceByKey(
        # Reduce to keep the latest time_visited.  Schema is now (unique username, city_visited), last_visited.
        lambda x, y: max(x, y)
    ).map(
        # Schema is now (city_visited), 1, last_visited (one row per unique user)
        lambda row: ((row[0][1]), (1, row[1]))
    ).reduceByKey(
        # Reduce to get the number of unique visitors and last visited time for the city.
        # Schema is now (city_visited), num of unique visits, last_visited
        lambda x, y: (x[0] + y[0], max(x[1], y[1]))
    )
    return results_rdd.takeOrdered(num_of_results, lambda (k, v): v[0])

if __name__ == "__main__":
    """
        Find cities that are the least visited by unique users.
        Usage: leastVisitedCities [number of results to return]
    """

    num_of_results = int(sys.argv[1]) if len(sys.argv) > 1 else DEFAULT_NUM_OF_RESULTS
    spark_context = SparkContext(appName="leastVisitedCities")
    visited_cities_dataframe = setupDataFrame(spark_context)

    time1 = time.time()
    results = getLeastVisitedNCities(visited_cities_dataframe.rdd, num_of_results)
    time2 = time.time()

    print('finished in %0.3f ms'%((time2-time1)*1000.0))

    print('-------- Results: --------------------')
    for result in results:
      print('%s visited by %i unique users, last visited at %s'%(result[0], result[1][0], str(result[1][1])))

    spark_context.stop()